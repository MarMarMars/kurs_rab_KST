package com.kurs.products.repo;

import com.kurs.products.models.PrinodOfProducts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

public interface PrinodOfProductsRepository  extends CrudRepository<PrinodOfProducts, Long> {

    @Query("Select SUM(p.sum) from PrinodOfProducts p")
    Iterable<PrinodOfProducts> findSumPrihod();

}

