package com.kurs.products.models;

import javax.persistence.*;

@Entity
@Table(name = "products_table")
public class ProductsTable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name_products,description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName_products() {
        return name_products;
    }

    public void setName_products(String name_products) {
        this.name_products = name_products;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductsTable() {
    }

    public ProductsTable(String name_products, String description) {

        this.name_products = name_products;
        this.description = description;

    }
}
