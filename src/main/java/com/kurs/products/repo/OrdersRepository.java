package com.kurs.products.repo;

import com.kurs.products.models.Orders;
import com.kurs.products.models.PrinodOfProducts;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface OrdersRepository extends CrudRepository<Orders, Long> {

    @Query("Select SUM(p.kol * p.price) from Orders p")
    Iterable<Orders> findSumProd();

}
