package com.kurs.products.controllers;

import com.kurs.products.models.Orders;
import com.kurs.products.models.ProductsTable;
import com.kurs.products.repo.OrdersRepository;
import com.kurs.products.repo.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class OrdersController {

    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private ProductsRepository productsRepository;

    @GetMapping("/orders")
    public String ordersMain (Model model){
        model.addAttribute("title","Заказы клиентов" );
        Iterable<Orders> orderstable = ordersRepository.findAll();
        model.addAttribute("orders",orderstable);
        return "orders";
    }

    @GetMapping("/orders/add")
    public String ordersadd(Model model) {

        model.addAttribute("title","Создание заказа клиента" );
        Iterable<ProductsTable> prodId = productsRepository.findIdAllProducts();
        model.addAttribute("prodId",prodId);
        Iterable<Orders> orderstable = ordersRepository.findAll();
        model.addAttribute("orders",orderstable);

        return "orders-add";
    }

    @PostMapping("/orders/add")
    public String ordersadd(@RequestParam Integer id_products, @RequestParam Integer kol, @RequestParam Integer price) {

        Orders orderstable = new Orders(id_products, kol, price);
        ordersRepository.save(orderstable);

        return "redirect:/orders";
    }

    @GetMapping("/orders/{id_orders}/edit")
    public String ordersedit(@PathVariable( value = "id_orders") long id,  Model model) {
        model.addAttribute("title","Редактирование заказа" );
        Optional<Orders> orderstable = ordersRepository.findById(id);
        String str = "Заказ "+ orderstable.get().getId();

        model.addAttribute("nameorders",str);
        model.addAttribute("orders",orderstable.get());
        model.addAttribute("id_orders",id);

        return "orders-edit";
    }

    @PostMapping("/orders/{id_orders}/edit")
    public String orderspostedit(@PathVariable( value = "id_orders") long id, @RequestParam Integer id_products, @RequestParam Integer kol, @RequestParam Integer price, Model model) {

        Orders orderstable = ordersRepository.findById(id).orElseThrow(IllegalStateException::new);

        orderstable.setId(id);
        orderstable.setId_product(id_products);
        orderstable.setKol(kol);
        orderstable.setPrice(price);

        ordersRepository.save(orderstable);

        return "redirect:/orders";
    }

    @PostMapping("/orders/{id_orders}/remove")
    public String orderspostremove(@PathVariable( value = "id_orders") long id, Model model) {

        Orders orderstable = ordersRepository.findById(id).orElseThrow(IllegalStateException::new);
        ordersRepository.delete(orderstable);

        return "redirect:/orders";
    }
}
