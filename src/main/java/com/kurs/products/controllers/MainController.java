package com.kurs.products.controllers;

import com.kurs.products.models.PrinodOfProducts;
import com.kurs.products.repo.PrinodOfProductsRepository;
import com.kurs.products.models.ProductsTable;
import com.kurs.products.repo.ProductsRepository;
import com.kurs.products.models.Orders;
import com.kurs.products.repo.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Controller
public class MainController {

    @Autowired
    private PrinodOfProductsRepository prinodOfProductsRepository;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private ProductsRepository productsRepository;

    //@RequestParam(name="name", required=false, defaultValue="World") String name,
    @GetMapping("/")
    public String home(Model model) {
        Iterable<PrinodOfProducts> sumtable = prinodOfProductsRepository.findSumPrihod();
        Iterable<Orders> sumOrders = ordersRepository.findSumProd();
        Iterable<ProductsTable> prodost = productsRepository.findOstProducts();
        model.addAttribute("title","Главная страница" );
        model.addAttribute("sumrash",sumtable);
        model.addAttribute("sumprod",sumOrders);
        model.addAttribute("ostatok",prodost);
        return "home";
    }



}
