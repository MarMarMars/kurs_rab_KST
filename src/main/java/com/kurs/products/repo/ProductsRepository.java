package com.kurs.products.repo;

import com.kurs.products.models.ProductsTable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductsRepository extends CrudRepository<ProductsTable, Long> {

    @Query("Select p.id, p.name_products, SUM(COALESCE(r.kol,0) - COALESCE(o.kol,0)), SUM(COALESCE(r.kol,0)), SUM(COALESCE(o.kol,0)) from" +
            " ProductsTable p LEFT OUTER JOIN PrinodOfProducts r on p.id = r.id_product LEFT OUTER JOIN Orders o on p.id = o.id_product" +
            " group by p.id, p.name_products")
    Iterable<ProductsTable> findOstProducts();

    @Query("Select p.id from ProductsTable p order by p.id")
    Iterable<ProductsTable> findIdAllProducts();
}
