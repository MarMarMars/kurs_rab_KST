package com.kurs.products.controllers;

import com.kurs.products.models.PrinodOfProducts;
import com.kurs.products.models.ProductsTable;
import com.kurs.products.repo.PrinodOfProductsRepository;
import com.kurs.products.repo.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class PrihodController {

    @Autowired
    private PrinodOfProductsRepository prinodOfProductsRepository;
    @Autowired
    private ProductsRepository productsRepository;

    @GetMapping("/prihod/add")
    public String prihodsadd(Model model) {

        model.addAttribute("title","Создание приобретения товаров" );

        Iterable<PrinodOfProducts> prinodtable = prinodOfProductsRepository.findAll();
        Iterable<ProductsTable> prodId = productsRepository.findIdAllProducts();
        model.addAttribute("prodId",prodId);
        model.addAttribute("prinodtable",prinodtable);

        return "prihod-add";
    }

    @PostMapping("/prinod/add")
    public String prihodadd(@RequestParam Integer id_products, @RequestParam Integer kol, @RequestParam Integer sum) {

        PrinodOfProducts prinodtable = new PrinodOfProducts(id_products, kol, sum);
        prinodOfProductsRepository.save(prinodtable);

        return "redirect:/home";
    }

}
