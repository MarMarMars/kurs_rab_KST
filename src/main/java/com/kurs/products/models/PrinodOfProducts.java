package com.kurs.products.models;

import javax.persistence.*;

@Entity
@Table(name = "prihod_of_products")
public class PrinodOfProducts {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private int kol, sum, id_product;
    //private Date date_order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getKol() {
        return kol;
    }

    public void setKol(int kol) {
        this.kol = kol;
    }

    public int getPrice() {
        return sum;
    }

    public void setPrice(int sum) {
        this.sum = sum;
    }


    public PrinodOfProducts() {
    }

    public PrinodOfProducts(int id_product, int kol, int sum) {

        this.id_product = id_product;
        this.kol = kol;
        this.sum = sum;
        //this.date_order = date_order;
    }
}
