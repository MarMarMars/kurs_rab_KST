package com.kurs.products.models;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private int kol, price, id_product;
    //private Date date_order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getKol() {
        return kol;
    }

    public void setKol(int kol) {
        this.kol = kol;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public Orders() {
    }

    public Orders(int id_product, int kol, int price) {

        this.id_product = id_product;
        this.kol = kol;
        this.price = price;
        //this.date_order = date_order;
    }
}
