package com.kurs.products.controllers;

import com.kurs.products.models.ProductsTable;
import com.kurs.products.repo.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class BlogController {

    @Autowired
    private ProductsRepository productsRepository;

    @GetMapping("/products")
    public String productsMain (Model model){
        model.addAttribute("title","Товары" );
        Iterable<ProductsTable> productstable = productsRepository.findAll();
        model.addAttribute("products",productstable);
        return "products";
    }

    @GetMapping("/products/add")
    public String productsadd(Model model) {

        model.addAttribute("title","Добавление номенклатуры" );

        Iterable<ProductsTable> productstable = productsRepository.findAll();
        model.addAttribute("products",productstable);

        return "products-add";
    }

    @PostMapping("/products/add")
    public String productsadd(@RequestParam String description, @RequestParam String name_products) {

        ProductsTable products = new ProductsTable(name_products, description);
        productsRepository.save(products);

        return "redirect:/products";
    }

    @GetMapping("/products/{id_products}")
    public String productsDetails(@PathVariable( value = "id_products") long id, Model model) {

        model.addAttribute("title","Подробнее о номенклатуре" );

        Optional<ProductsTable> productstable = productsRepository.findById(id);
        String str = "Номенклатура "+ productstable.get().getName_products() + " (артикул: "+ productstable.get().getId() + ")";

        model.addAttribute("nameproducts",str);
        model.addAttribute("description_products",productstable.get().getDescription());
        model.addAttribute("id_products",id);

        return "products-full";
    }

    @GetMapping("/products/{id_products}/edit")
    public String productsedit(@PathVariable( value = "id_products") long id,  Model model) {
        model.addAttribute("title","Редактирование номенклатуры" );
        Optional<ProductsTable> productstable = productsRepository.findById(id);
        String str = "Номенклатура "+ productstable.get().getName_products();

        model.addAttribute("nameproducts",str);
        model.addAttribute("products",productstable.get());
        model.addAttribute("id_products",id);

        return "products-edit";
    }

    @PostMapping("/products/{id_products}/edit")
    public String roomspostedit(@PathVariable( value = "id_room") long id, @RequestParam Long id_products, @RequestParam Integer cost, @RequestParam String name_products, @RequestParam String description, Model model) {

        ProductsTable productstable = productsRepository.findById(id).orElseThrow(IllegalStateException::new);

        productstable.setId(id_products);
        productstable.setName_products(name_products);
        productstable.setDescription(description);

        productsRepository.save(productstable);

        return "redirect:/products";
    }

    @PostMapping("/products/{id_products}/remove")
    public String productspostremove(@PathVariable( value = "id_products") long id, Model model) {

        ProductsTable productstable = productsRepository.findById(id).orElseThrow(IllegalStateException::new);
        productsRepository.delete(productstable);

        return "redirect:/products";
    }
}
